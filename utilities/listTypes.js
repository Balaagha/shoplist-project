export const LISTS_TYPE = Object.freeze({
    ONETIMELIST:"oneTimeList",
    REGULARLIST:"regularList"
});

export function getListTypeFromRoute(route){
    return route?.params?.listType || LISTS_TYPE.ONETIMELIST;
}

