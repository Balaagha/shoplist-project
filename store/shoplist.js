import { AsyncStorage } from 'react-native'

// ACTION TYPES
import { SET_APP_DATA } from '../api';
const ADD_LIST = "ADD_LIST";
const DELETE_LIST = "DELETE_LIST";
const SET_LISTS = "SET_LISTS";
const ADD_LISTITEM = "ADD_LISTITEM";
const UPDATE_LIST_ITEM = "UPDATE_LISTITEM";
const DELETE_LIST_ITEM = "DELETE_LIST_ITEM";
const COMPLATE_LIST_ITEM = "COMPLATE_LIST_ITEM";
const RESET_LIST = "RESET_LIST";


// SELECTORS
export const MODULE_NAME = "shoplist";
export const getListByType = (state,type) => state[MODULE_NAME][type];
export const getSingleListById = (state,type,id) => getListByType(state,type).find((list)=>list.id===id);
// REDUCER
const initialState = { regularList: [],oneTimeList:[],};



export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_APP_DATA:
      return {...state,...payload[MODULE_NAME]}
    case SET_LISTS:
    return{...state,...payload}
    case ADD_LIST:
      return {...state,
        [payload.listType]:[
          ...state[payload.listType],{id: `${Math.random()}${Date.now()}`,name: payload.name,shoplist:[]}
        ]
      }
    case DELETE_LIST:
      return { 
        ...state,
        [payload.listType]:[
          ...state[payload.listType].filter(item=>item.id!==payload.listID)
        ] 
      };
     case RESET_LIST:
       console.log('resetde')
       return {...state,
        [payload.listType]:state[payload.listType].map((list)=>{
          if(list.id === payload.listID){ 
            return {
              ...list,
              shoplist:list.shoplist.map((shopItem)=>{
                 return {
                    ...shopItem,done:false
                  }
              })
            }
          } return list;
        })
      } 
    case ADD_LISTITEM:
      return {...state,
        [payload.listType]:state[payload.listType].map((list)=>{
          if(list.id === payload.listID){ 
            return {
              ...list,
              shoplist:[...list.shoplist,
                {id:`${Math.random()}${Date.now()}`,name: payload.name,type:payload.type,count:payload.count,done:false}
              ]
            }
          } return list;
        })
      } 
      case UPDATE_LIST_ITEM:
        return {...state,
          [payload.listType]:state[payload.listType].map((list)=>{
            if(list.id === payload.listID){ 
              return {
                ...list,
                shoplist:list.shoplist.map((shopItem)=>{
                  if(shopItem.id === payload.itemID){
                   return {
                      ...shopItem, name: payload.name,type:payload.type,count:payload.count,done:false
                    }
                  }return shopItem;
                })
              }
            } return list;
          })
        } 
    case DELETE_LIST_ITEM:
      return {...state,
        [payload.listType]:state[payload.listType].map((list)=>{
          if(list.id === payload.listID){ 
            return {
              ...list,
              shoplist:list.shoplist.filter((shopItem)=>shopItem.id!==payload.itemID)
            }
          } return list;
        })
      } 
    case COMPLATE_LIST_ITEM:
      return {...state,
        [payload.listType]:state[payload.listType].map((list)=>{
          if(list.id === payload.listID){ 
            return {
              ...list,
              shoplist:list.shoplist.map((shopItem)=>{
                if(shopItem.id === payload.itemID){
                 return { ...shopItem, done:!shopItem.done }
                }return shopItem;
              })
            }
          } return list;
        })
      } 

    default: return state;
  }
}

// ACTION CREATORS
export const deleteList = (payload) => ({ type: DELETE_LIST, payload });
export const addList = (payload) => ({ type: ADD_LIST, payload });
export const setLists = (payload) => ({ type: SET_LISTS, payload });
export const resetList = (payload) => ({ type: RESET_LIST, payload });
export const addListItem = (payload) => ({ type: ADD_LISTITEM, payload });
export const updateListItem = (payload) => ({ type: UPDATE_LIST_ITEM, payload });
export const deleteListItem = (payload) => ({ type: DELETE_LIST_ITEM, payload });
export const complateListItem = (payload) => ({ type: COMPLATE_LIST_ITEM, payload });





/* 

const initialState = { 
  regularList: [
    
    {
      id: `${Math.random()}${Date.now()}`,
      name: "List 2",
      shoplist: [
        { id:`${Math.random()}${Date.now()}`, name: "pasta",type:'pkg',count:2,done:true },
        { id:`${Math.random()}${Date.now()}`, name: "Cheese",type:'kg',count:1,done:true },
      ],
    },
  ],
  oneTimeList:[
    {
      id: `${Math.random()}${Date.now()}`,
      name: "List 3",
      shoplist: [
        { id:`${Math.random()}${Date.now()}`, name: "pasta",type:'pkg',count:2,done:true },
        { id:`${Math.random()}${Date.now()}`, name: "Cheese",type:'kg',count:1,done:true },
      ],
    },
    {
      id: `${Math.random()}${Date.now()}`,
      name: "List 4",
      shoplist: [
        { id:`${Math.random()}${Date.now()}`, name: "pasta",type:'pkg',count:3,done:false },
        { id:`${Math.random()}${Date.now()}`, name: "Salt",type:'pkg',count:3,done:true },
        { id:`${Math.random()}${Date.now()}`, name: "Cheese",type:'kg',count:1,done:true },
      ],
    },
  ]
};



*/