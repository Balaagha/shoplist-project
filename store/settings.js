// ACTION TYPES
import { SET_APP_DATA } from '../api';
const UPDATE_USER = "UPDATE_USER";

export const MODULE_NAME = "settings";
export const getUserSettings = (state) => state[MODULE_NAME].userSettings;

// REDUCER
const initialState = { userSettings: {userName:'Username',avatarUrl:''} };
export function reducer(state=initialState,{type,payload}){
    switch(type){
        case SET_APP_DATA:
            return {...state,...payload[MODULE_NAME]}
        case UPDATE_USER:
            return {...state,userSettings:{userName:payload.userName,avatarUrl:payload.avatarUrl}} 
        default:
            return state;
    }
}

//ACTION CREATOR
export const updateUser = (payload) => ({ type: UPDATE_USER, payload });


