// ACTION TYPES
import { SET_APP_DATA } from '../api';
import { setLists } from './shoplist';
const SET_AUTH_DATA = "SET_AUTH_DATA";
const AUTH_LOGOUT = "AUTH_LOGOUT";

export const MODULE_NAME = "auth";
export const getUserAuthData = (state) => state[MODULE_NAME];

// REDUCER
const initialState = { email:null,token:null,refreshToken:null,tokenExpries:0,userID: null};
export function reducer(state=initialState,{type,payload}){
    switch(type){
        case SET_APP_DATA:
            return {...state,...payload[MODULE_NAME]}
        case SET_AUTH_DATA:
            return {...state,email:payload.email,token:payload.token,refreshToken:payload.refreshToken,tokenExpires:payload.tokenExpires,userID: payload.userID} 
        case AUTH_LOGOUT:
            return {...state,...initialState}
        default:
            return state;
    }
}

//ACTION CREATOR
export const setAuthData = payload => ({type:SET_AUTH_DATA,payload});
export const authLogOut = () => ({type:AUTH_LOGOUT});
 

//MIDLEWARE
const API_KEY = 'AIzaSyBBBmRtzCZZtwJ3HuuZEa49kGQKT6SGhgs';
export const singUp = (email,password) => async dispatch => {
    console.log('singUp da')
    try {
        const response = await fetch(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${API_KEY}`,
            {method:'POST',headers:{'Content-Type':'application/json'},body: JSON.stringify({returnSecureToken:true,email,password})}
            );
        const result = await response.json();
        console.log(result);
        if(!result.error){
            dispatch(
                setAuthData({
                    token: result.idToken,
                    refreshToken: result.refreshToken,
                    tokenExpires: Date.now() + (result.expiresIn + 1000) - 15000,
                    email: result.email,
                    userID: result.localId

                })
            );
        }
    } catch(error){console.log('sing up error',error);}
}

export const singIn = (email,password) => async dispatch => {
    console.log('singIn de')
    try {
        const response = await fetch(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${API_KEY}`,
            {method:'POST',headers:{'Content-Type':'application/json'},body: JSON.stringify({returnSecureToken:true,email,password})}
            );
        const result = await response.json();
        console.log(result);
        if(!result.error){
            dispatch(
                setAuthData({
                    token: result.idToken,
                    refreshToken: result.refreshToken,
                    tokenExpires: Date.now() + (result.expiresIn + 1000) - 15000,
                    email: result.email,
                    userID: result.localId
                })
            );
        }
    } catch(error){console.log('sing up error',error);}
}

export const saveData = (email,password) => async (dispatch,getState) =>  {
    try{
        const {shoplist,auth:{userID,token}} = getState();
        const response = await fetch(`https://photo-geo-md-3.firebaseio.com/shoplist/${userID}.json?auth=${token}`,
        {method:'PUT',headers:{'Content-Type':'application/json'},body:JSON.stringify(shoplist)},
        );
        const result = await response.json();
        if(!result.error){alert('Successfully save data');}else{ alert('Failed data saved');}
    } catch(err){console.log('saveData error',err);}
}


export const loadData = () => async (dispatch,getState) =>  {
    try{
        const {auth:{userID,token}} = getState();
        const response = await fetch(`https://photo-geo-md-3.firebaseio.com/shoplist/${userID}.json?auth=${token}`);
        const result = await response.json();        
        if(!result.error){alert('All data loaded');dispatch(setLists(result)); }else{ alert('Failed data loaded');}
    } catch(err){console.log('loadData error',err);}
}



