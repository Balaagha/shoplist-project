import { createStore, combineReducers, applyMiddleware } from "redux";
import Thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { reducer as shoplistReducer,MODULE_NAME as shoplistModuleName } from "./shoplist";
import { reducer as userSettingsReducer,MODULE_NAME as userSettingsModuleName } from "./settings";
import { reducer as authReducer,MODULE_NAME as authModuleName } from "./auth";

import { getAppDataFromAs,updateAppData } from "../api";

const rootReducer = combineReducers({
    [shoplistModuleName]:shoplistReducer,
    [userSettingsModuleName]:userSettingsReducer,
    [authModuleName]:authReducer
});

export const store = createStore(rootReducer,composeWithDevTools(applyMiddleware(Thunk)));

store.subscribe(()=>updateAppData(store));
getAppDataFromAs(store);
export default store;
