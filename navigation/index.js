import React from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { HomeStack } from './HomeStack';
import { UserSettingsStack } from "./UserSettingsStack";
import { UserAuthStack } from "./UserAuthStack";
import { CreateListStack } from "./CreateListStack";
import CustomDrawerContent from '../common/CustomDrawerContent';

const { Navigator, Screen } = createDrawerNavigator();
import {connect} from "react-redux";
import {getUserSettings} from '../store/settings';
const mapStateToProps =(state) =>({userSettings:getUserSettings(state)});

export const RootNav = connect(mapStateToProps)((props) => {
    const {userName,avatarUrl} = props.userSettings;
    return (
      <NavigationContainer>
        <Navigator drawerContent={props => <CustomDrawerContent {...props} avatarUrl={avatarUrl} userName={userName} />}>
          <Screen name="HomeStack" component={HomeStack} />
          <Screen name="UserSettingsStack" component={UserSettingsStack} />
          <Screen name="UserAuthStack" component={UserAuthStack} />
          <Screen name="CreateList" component={CreateListStack} />
        </Navigator>
      </NavigationContainer>
      ) 
  
});
