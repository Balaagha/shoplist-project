import React from "react";
import { CreateList } from "../screens";

import { createStackNavigator } from "@react-navigation/stack";
import { headerDefaultStyles } from "../styles/headerDefaultStyles";
const { Navigator, Screen } = createStackNavigator();

export const CreateListStack = () => (
  <Navigator>
    <Screen name="CreateList" component={CreateList} options={{...headerDefaultStyles,title:'New List'}} />
  </Navigator>
);
