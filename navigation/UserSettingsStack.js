import React from "react";
import COLORS from "../styles/colors";
import { UserSettings } from "../screens";

import { createStackNavigator } from "@react-navigation/stack";
import { headerDefaultStyles } from "../styles/headerDefaultStyles";
const { Navigator, Screen } = createStackNavigator();

export const UserSettingsStack = () => (
  <Navigator>
    <Screen name="UserSettings" component={UserSettings} options={{...headerDefaultStyles,title:'User Settings'}} />
  </Navigator>
);
