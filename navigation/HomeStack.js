import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { SingleList, HomeScreen } from "../screens";
import { LISTS_TYPE } from "../utilities/listTypes";
import { HeaderCustomBtn } from "../components";
import { headerDefaultStyles } from "../styles/headerDefaultStyles";

const { Navigator, Screen } = createStackNavigator();
export const HomeStack = () => (
  <Navigator screenOptions={headerDefaultStyles}>
    <Screen name="Home" component={HomeScreen} options={({route,navigation})=>({
        title:getHomeStackTitleFromRoute(route.params?.listType),headerTitleStyle:{fontSize:16,fontWeight: '600',fontFamily:'MontserratMedium'},
        headerRight:()=>( <HeaderCustomBtn  type="openMenu" onPress={()=>navigation.openDrawer()} /> ),
    })} />
    <Screen name="SingleList" component={SingleList} options={({route,navigation})=>({
      title:route.params.name,
      headerRight:()=>(
        <HeaderCustomBtn  type="sigleItemHeaderIcon" editMode={route.params.editMode} onPress={()=>navigation.setParams({editMode:!route.params.editMode})} />
      ),
      })} />
  </Navigator>
);

function getHomeStackTitleFromRoute (type){
    const titles = Object.freeze({  [LISTS_TYPE.ONETIMELIST]:'One Time Lists',[LISTS_TYPE.REGULARLIST]:'Regular Lists', });
    return titles[type] || titles[LISTS_TYPE.ONETIMELIST];
}
