import React from "react";
import COLORS from "../styles/colors";
import { UserAuth } from "../screens";

import { createStackNavigator } from "@react-navigation/stack";
import { headerDefaultStyles } from "../styles/headerDefaultStyles";
import { HeaderCustomBtn } from "../components";
const { Navigator, Screen } = createStackNavigator();

export const UserAuthStack = () => (
  <Navigator>
    <Screen name="UserAuth" component={UserAuth} options={({navigation})=>({...headerDefaultStyles,title:"User Auth",
      headerRight:()=>( <HeaderCustomBtn  type="openMenu" onPress={()=>navigation.openDrawer()} /> ),})} />
  </Navigator>
);
