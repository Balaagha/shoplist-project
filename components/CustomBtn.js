import React from "react";
import {StyleSheet,View,TouchableOpacity,Platform,Text,TouchableNativeFeedback} from "react-native";
import COLORS from "../styles/colors";
const fontFamilies = {regular: "MontserratRegular",medium: "MontserratMedium",bold: "MontserratBold"};
export const CustomBtn = ({title,onPress,color,fontSize,style,...rest}) => {
  const Touchable = Platform.OS === "android" ? TouchableNativeFeedback : TouchableOpacity;
  
  return (
      <Touchable onPress={onPress} style={{borderWidth:2}} {...rest}>
        <View style={[styles.btn, style]}>
          <Text style={[styles.title,{color: color ? color : "white",fontSize: fontSize ? fontSize : 14} ]}>{title}</Text>
        </View>
      </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {width: "100%",justifyContent: "center",overflow:"hidden",height:42},
  btn: {width:"100%",borderRadius:50,backgroundColor:COLORS.main,alignItems:"center",justifyContent:'center',height:42},
  title: {textAlign: "center",fontFamily:'MontserratBold',fontSize:14},
});
