import React from 'react';
import { Text,TouchableOpacity } from 'react-native';

import { MaterialIcons } from '@expo/vector-icons'; 
import { Feather } from '@expo/vector-icons';

export const HeaderCustomBtn = (props) => {
    let headerRightIcon=null;
    if(props.type === 'sigleItemHeaderIcon'){
        headerRightIcon = props.editMode ? <Feather name="save" size={24} color="white" /> :<MaterialIcons name="edit" size={24} color="white" />;
    } else if(props.type === 'openMenu'){
        headerRightIcon =  <Feather name="menu" size={35} color="white" />
    }
    
    return (
        <TouchableOpacity style={{marginRight:10}} onPress={props.onPress}>
            <Text>{headerRightIcon}</Text>
        </TouchableOpacity>
    )
}

