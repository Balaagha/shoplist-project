import React from 'react'
import {View,StyleSheet,TouchableWithoutFeedback,Keyboard } from 'react-native'
import COLORS from '../styles/colors';

const Layout = ({children}) => {
    return (
        <View style={styles.wrapperContainer}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.container}>{children}</View>
            </TouchableWithoutFeedback>
        </View>
        
    )
}
const styles = StyleSheet.create({
    wrapperContainer:{backgroundColor:COLORS.main,flex:1},
    container:{backgroundColor:'#fff',flex:1,borderTopStartRadius:20,borderTopEndRadius:20,padding:16},
});
export default Layout
