import React from 'react';
import { StyleSheet, View, Image,Text } from "react-native";
import COLORS from '../styles/colors';
import { CustomBtn } from '../components';
import { LISTS_TYPE } from '../utilities/listTypes';

const CustomDrawerContent = (props) => {
  const {userName,avatarUrl} =props;
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          resizeMode='cover'
          source={{
            uri: (avatarUrl!=='' ) ? avatarUrl : "https://www.searchpng.com/wp-content/uploads/2019/02/Deafult-Profile-Pitcher.png",
          }}
          style={styles.userImg}
        />
        <Text style={styles.userText}>{userName}</Text>
      </View>
      <View style={styles.menuContainer}>
        <CustomBtn onPress={() => props.navigation.navigate("CreateList")} style={[styles.menuBtn,{marginBottom:32}]} fontSize={14} color={COLORS.main} title="ADD NEW LIST"/>
        <CustomBtn onPress={() => props.navigation.navigate("Home",{listType:LISTS_TYPE.ONETIMELIST})} style={styles.menuBtn} fontSize={14} color={COLORS.main} title="ONE TIME LISTS"/>
        <CustomBtn onPress={() => props.navigation.navigate("Home",{listType:LISTS_TYPE.REGULARLIST})} style={styles.menuBtn} fontSize={14} color={COLORS.main} title="REGULAR LISTS"/>
        <CustomBtn onPress={() => props.navigation.navigate("UserSettingsStack")} style={styles.menuBtn} fontSize={14} color={COLORS.main} title="USER SETTINGS"/>
        <CustomBtn onPress={() => props.navigation.navigate("UserAuthStack")} style={styles.menuBtn} fontSize={14} color={COLORS.main} title="USER AUTH"/>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {backgroundColor: "white",},
  headerContainer: {flexDirection: "row",alignItems:'center',paddingHorizontal:16,height:73,},
  userImg: {width: 52,height: 50,borderColor: COLORS.main,borderRadius: 25,borderWidth: 3},
  userText: {fontSize: 24,fontFamily: "MontserratRegular",marginLeft: 22,},
  menuContainer: {height:'100%',padding:16,backgroundColor: COLORS.main,borderTopStartRadius: 18,borderTopEndRadius: 18,},
  menuBtn:{height:34,backgroundColor:'white',marginBottom:10}
});
export default CustomDrawerContent

