import React,{useState} from 'react';
import {StyleSheet,Text,TextInput,View,Alert } from 'react-native';
import Layout from '../common/Layout';
import COLORS from '../styles/colors';
import { CustomBtn } from '../components';

import { connect } from 'react-redux';
import { getUserSettings,updateUser } from '../store/settings';

import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions'; 
import { LISTS_TYPE } from '../utilities/listTypes';

const mapStateToProps = (state) => { 
    return {userSettings:getUserSettings(state)}; 
};

export const UserSettings = connect(mapStateToProps,{updateUser})(({navigation,userSettings,updateUser}) => {
    const [fields,setFields] = useState({userName:userSettings.userName,avatarUrl:userSettings.avatarUrl});
    const fieldsChangeHandler=(name,value)=>{setFields(()=>({...fields,[name]:value}))}
    const createUserHander=()=>{
        updateUser(fields);
        if(fields.userName===userSettings.userName && fields.avatarUrl===userSettings.avatarUrl){
            alert('You dont change anything');
        } else { alert('Changes are saved');}
        navigation.navigate("Home",{listType:LISTS_TYPE.ONETIMELIST});
    }
    const pickFromCamera = async ()=>{ 
        const {granted} = await Permissions.askAsync(Permissions.CAMERA_ROLL); 
        if(granted){ 
            let data = await ImagePicker.launchCameraAsync({
                mediaTypes:ImagePicker.MediaTypeOptions.Images,
                allowsEditing:true,aspect:[1,1],quality:0.5
            });
            if(!data.cancelled){ 
                fieldsChangeHandler('avatarUrl',data.uri)
            }else{Alert.alert('This operations was canceled');}
        }else{  Alert.alert('You have no permisions camera'); } 
    }
    const pickFromGalary = async ()=>{  
        const {granted} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if(granted){
            let data = await ImagePicker.launchImageLibraryAsync({
                mediaTypes:ImagePicker.MediaTypeOptions.Images,allowsEditing:true,aspect:[1,1],quality:0.5
            });
            if(!data.cancelled){ 
                fieldsChangeHandler('avatarUrl',data.uri)
            }else{Alert.alert('This operations was canceled');}
        }else{  Alert.alert('You have no permisions camera'); } 
    }
    const uploadAvatar =()=>{
        Alert.alert('','Take image from galery or choose camera',
            [{ text: 'Camera', onPress: () => pickFromCamera(),},{ text: 'Galery', onPress: () => pickFromGalary()}],
            { cancelable: true }
          );     
    }
    return (
        <Layout>
            <Text style={styles.inputLabel}>username</Text>
            <TextInput onChangeText={(value)=>fieldsChangeHandler('userName',value)} value={fields.userName} style={styles.input}/>
            <Text style={[styles.inputLabel,{marginTop:10}]}>avatar url</Text>
            <TextInput onChangeText={(value)=>fieldsChangeHandler('avatarUrl',value)} value={fields.avatarUrl} style={styles.input}/>
            <CustomBtn onPress={uploadAvatar} color={COLORS.black} style={{ marginTop: 15,backgroundColor:COLORS.gray }} title="CHOOSE IMAGE"/>
            <CustomBtn onPress={createUserHander} style={{ marginTop: 15 }} title="SAVE CHANGES"/>
        </Layout>
    )
})
const styles = StyleSheet.create({  
    inputLabel:{textAlign:'center',fontFamily:'MontserratMedium',fontSize:12,color:COLORS.black,marginBottom:4},
    input:{backgroundColor:COLORS.gray,height:42,borderRadius:45,borderColor:'#fff',textAlign:'center',
        fontFamily:'MontserratMedium',fontSize:18},
});


