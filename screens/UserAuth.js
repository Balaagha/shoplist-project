import React,{useState} from 'react';
import {StyleSheet,Text,TextInput,View,Alert } from 'react-native';
import Layout from '../common/Layout';
import COLORS from '../styles/colors';
import { CustomBtn } from '../components';

import { connect } from 'react-redux';
import { getUserAuthData,singIn,singUp,saveData,loadData,authLogOut } from '../store/auth';
import { TouchableOpacity } from 'react-native-gesture-handler';

const mapStateToProps = (state) => { 
    return {userAuth:getUserAuthData(state)}; 
};
const fieldsInitialState = {email:'',password:'',rePassword:'',authMode:false}
export const UserAuth = connect(mapStateToProps,{singIn,singUp,saveData,loadData,authLogOut})(({navigation,userAuth,singIn,singUp,saveData,loadData,authLogOut}) => {
    const [fields,setFields] = useState(fieldsInitialState);
    const fieldsChangeHandler=(name,value)=>{setFields(()=>({...fields,[name]:value}))}
    const authModeChangeHandler =()=>{setFields(()=>({...fields,authMode:!fields.authMode}))}
    const {email,userID} = userAuth;
    const singUpHandler=()=>{
        if(fields.email.trim()==='' && fields.password.trim()===''){
            alert('You dont write email or password');return;
        } else  if(fields.password !==fields.rePassword){
            alert('Password dont match!');return;
        }
        singUp(fields.email,fields.password);
        setFields(fieldsInitialState);
    }
    const singInHandler=()=>{
        if(fields.email.trim()==='' && fields.password.trim()===''){
            alert('You dont write email or password');return;
        } 
        singIn(fields.email,fields.password);
        setFields(fieldsInitialState);
    }
    // const uploadAvatar =()=>{
    //     Alert.alert('','Take image from galery or choose camera',
    //         [{ text: 'Camera', onPress: () => pickFromCamera(),},{ text: 'Galery', onPress: () => pickFromGalary()}],
    //         { cancelable: true }
    //       );     
    // }
    return (
        <Layout>
        { email? 
        <>
            
            <View style={styles.row}>
                <Text style={styles.userInfoHeadind}>User mail :</Text>
                <Text style={styles.userInfoMail}>{email}</Text>
            </View>
            <CustomBtn onPress={saveData} color={COLORS.black} style={{ marginTop: 15,backgroundColor:COLORS.gray }} title="SAVE DATA"/>
            <CustomBtn onPress={loadData} color={COLORS.black} style={{ marginTop: 15,backgroundColor:COLORS.gray }} title="LOAD DATA"/>
            <CustomBtn onPress={()=>authLogOut()} style={{ marginTop: 15 }} title="LOG OUT"/>
        </>    
        :    
        <>
            <Text style={styles.inputLabel}>Email</Text>
            <TextInput onChangeText={(value)=>fieldsChangeHandler('email',value)} value={fields.email} style={styles.input}/>
            <Text style={[styles.inputLabel,{marginTop:10}]}>Password</Text>
            <TextInput onChangeText={(value)=>fieldsChangeHandler('password',value)} value={fields.password} style={styles.input}/>
            { fields.authMode ?
            <>
                <Text style={[styles.inputLabel,{marginTop:10}]}>Rewrite Password</Text>
                <TextInput onChangeText={(value)=>fieldsChangeHandler('rePassword',value)} value={fields.rePassword} style={styles.input}/>
                <CustomBtn onPress={singUpHandler} style={{ marginTop: 15 }} title="SING UP"/>                
                <View style={styles.authModeWrapper}>
                    <Text style={styles.authModeInfo}>Do you have account?</Text>
                    <TouchableOpacity onPress={authModeChangeHandler}><Text style={styles.authModeBtn}> Sing In</Text></TouchableOpacity>
                </View>
            </>    
                : 
            <>
                <CustomBtn onPress={singInHandler} style={{ marginTop: 15 }} title="SING IN"/>
                <View style={styles.authModeWrapper}>
                    <Text style={styles.authModeInfo}>You dont have any account?</Text>
                    <TouchableOpacity onPress={authModeChangeHandler}><Text style={styles.authModeBtn}> Sing Up</Text></TouchableOpacity>
                </View>
            </> 
            }
        </>   
        }
        </Layout>
    )
})
const styles = StyleSheet.create({  
    row:{flexDirection:'row',justifyContent:'space-evenly',alignItems:'center'},
    userInfoHeadind:{fontFamily:'MontserratBold',fontSize:16,},
    userInfoMail:{fontFamily:'MontserratMedium',fontSize:12,},
    authModeWrapper:{flexDirection:'row',justifyContent:"center",alignItems:'center',marginTop:16,},
    authModeInfo:{fontFamily:'MontserratMedium',fontSize:13,color:COLORS.textColor},
    authModeBtn:{fontFamily:'MontserratMedium',fontSize:13,color:COLORS.main},
    inputLabel:{textAlign:'center',fontFamily:'MontserratMedium',fontSize:12,color:COLORS.black,marginBottom:4},
    input:{backgroundColor:COLORS.gray,height:42,borderRadius:45,borderColor:'#fff',textAlign:'center',
        fontFamily:'MontserratMedium',fontSize:18},
});


