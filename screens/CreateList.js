import React,{useState} from 'react'
import {StyleSheet,Text,TextInput,View,Alert } from 'react-native'
import Layout from '../common/Layout'
import COLORS from '../styles/colors'
import { CustomBtn } from '../components'

import { connect } from 'react-redux';
import { addList } from '../store/shoplist'
import { LISTS_TYPE } from '../utilities/listTypes'

export const CreateList = connect(null,{addList})(({addList,navigation}) => {
    const [fields,setFields] = useState({listType:LISTS_TYPE.ONETIMELIST,name:''});
    const fieldsChangeHandler=(name,value)=>{setFields(()=>({...fields,[name]:value}))}
    const createUserHander=()=>{
        for(let key in fields){
            if(fields[key].trim() === ''){
                Alert.alert("Error!","Please write list name",[{ text: "OK"}],{ cancelable: false });
                return ;
            }
        }
        addList(fields);
        alert(`${fields.name} added to ${fields.listType}`);
        setFields({listType:'oneTimeList',name:''});
        navigation.navigate('Home',{listType:fields.listType});
    }
    return (
        <Layout>
            <Text style={styles.inputLabel}>List Name</Text>
            <TextInput onChangeText={(value)=>fieldsChangeHandler('name',value)} value={fields.name} style={styles.input}/>
            <View style={styles.listTypeWrapper}>
                <CustomBtn onPress={()=>fieldsChangeHandler("listType","oneTimeList")} style={[styles.listTypeBtn,{opacity:fields.listType === 'oneTimeList' ? 1 : 0.5}]} color={COLORS.black} fontSize={12} title="One Time"/>
                <CustomBtn onPress={()=>fieldsChangeHandler("listType","regularList")} style={[styles.listTypeBtn,{opacity:fields.listType === 'regularList' ? 1 : 0.5}]} color={COLORS.black} fontSize={12} title="Regular"/>
            </View>
            <CustomBtn onPress={createUserHander} style={{ marginTop: 12 }} title="CREATE LIST"/>
        </Layout>
    )
})
const styles = StyleSheet.create({  
    inputLabel:{textAlign:'center',fontFamily:'MontserratMedium',fontSize:12,color:COLORS.black,marginBottom:4},
    input:{backgroundColor:COLORS.gray,height:42,borderRadius:45,borderColor:'#fff',textAlign:'center',
        fontFamily:'MontserratMedium',fontSize:18},
    listTypeWrapper:{flexDirection:'row',justifyContent:'space-between',marginVertical:4},
    listTypeBtn:{marginTop: 10,width:'48%',backgroundColor:COLORS.gray}
});