import React from 'react';
import {View,Text,StyleSheet} from 'react-native';
import COLORS from '../../styles/colors';
import { ProgressBar } from './ProgressBar';
import { LISTS_TYPE } from '../../utilities/listTypes';

export const ListItem = ({list,listType}) => {
    let isComplete = false;
    const shoplistCount = list.shoplist.length;
    const isCompleteCount =  list.shoplist.filter(singleList=>singleList.done === true).length;
    const progressBarStatus = ((isCompleteCount/shoplistCount).toFixed(2))*100;
    if(listType===LISTS_TYPE.ONETIMELIST) {isComplete= isCompleteCount=== shoplistCount; }

    return (
        <View style={[styles.container,{opacity: isComplete ? 0.5 : 1}]}>
            <View style={styles.textContainer}> 
                <Text style={styles.textName}>{list.name}</Text>
                <Text style={styles.textCount}>{isCompleteCount} / {shoplistCount}</Text>
            </View>
            <ProgressBar progressBarStatus={progressBarStatus}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{borderRadius:10,borderWidth:2,borderColor:COLORS.primary,marginBottom:13,paddingHorizontal:20,paddingTop:11,paddingBottom:15},
    textContainer:{flexDirection:"row",justifyContent:"space-between",marginBottom:3},
    textName:{fontSize:18,fontFamily:'MontserratBold',},
    textCount:{fontSize:14,fontFamily:'MontserratBold',},
});