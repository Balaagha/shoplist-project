import React, { useEffect } from 'react';
import {TouchableOpacity,StyleSheet,FlatList,Alert  } from 'react-native';
import Layout from '../../common/Layout';

import { Feather } from '@expo/vector-icons'; 
import { ListItem } from './ListItem';

import {connect} from "react-redux";
import {getListByType, deleteList} from '../../store/shoplist';
import { getListTypeFromRoute } from '../../utilities/listTypes';

const mapStateToProps =(state,{route}) =>({ listData:getListByType(state,getListTypeFromRoute(route)) });

export const HomeScreen = connect(mapStateToProps,{deleteList})( ({navigation,route,listData,deleteList}) => {
    const listType = getListTypeFromRoute(route);

      const deleteListHandler=(listType,listID,listName)=>{
        Alert.alert('',`Do you want delete ${listName} ?`,
            [{ text: 'No', onPress: () => alert(`Operation was deleted`)},
            { text: 'Ok', onPress: () => {deleteList({listType,listID});alert(`${listName} was deleted`);}}],
            { cancelable: true }
          );  
      }
    return (
        <Layout>
            <FlatList 
                    data={listData} renderItem={({item})=> (
                        <TouchableOpacity onLongPress={()=>deleteListHandler(listType,item.id,item.name)} onPress={()=>navigation.navigate('SingleList',{listType,listID:item.id,name:item.name,editMode:false})}>
                            <ListItem listType={listType} key={item.id} list={item} />
                        </TouchableOpacity>
                    )}
                    keyExtractor={item => item.id}
                />
        </Layout>
                
    )
})
const styles = StyleSheet.create({
});

