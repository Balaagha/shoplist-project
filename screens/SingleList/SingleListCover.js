import React from 'react'
import { View,Text,StyleSheet,FlatList } from 'react-native'

export const SingleListCover = ({editMode,setEditData,editData,shoplist,onDeleteSingleItemHandler,onComplateSingleItemHandler}) => {
    return (
        <FlatList 
            data={shoplist}
            renderItem={({item})=>{
                return(<Text>{item.name} / </Text>)
            }}
            keyExtractor={item=>item.id}
        />
    )
}

const styles = StyleSheet.create({

});