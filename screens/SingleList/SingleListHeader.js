import React from 'react'
import { View,Text,StyleSheet } from 'react-native'
import { LISTS_TYPE } from '../../utilities/listTypes';
import { CustomBtn } from '../../components';

export const SingleListHeader = ({listType,onResetListHandler,singleListShoplist}) => {
    const shoplistCount = singleListShoplist.length;
    const isCompleteCount =  singleListShoplist.filter(singleList=>singleList.done === true).length;

    return (
        <View style={styles.headerWrapper}>
                <View style={styles.resetBtnWrapper} >
                    {
                        listType === LISTS_TYPE.REGULARLIST ?
                            <CustomBtn onPress={onResetListHandler}  style={styles.resetBtn} fontSize={10} title="RESET"/>
                        : null
                    }
                </View>
                <Text>{`${isCompleteCount} / ${shoplistCount}`}</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    headerWrapper:{flexDirection:'row',justifyContent:'space-between',marginBottom:14},
    resetBtnWrapper:{width:72},
    resetBtn:{height:20},
});
