import React,{useState} from 'react'
import { View, Text,TouchableOpacity,StyleSheet } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'; 
import { AntDesign } from '@expo/vector-icons'; 
import COLORS from '../../styles/colors';


export const SingleListItem = ({shopItem,editMode,setEditData,onComplateSingleItemHandler,onDeleteSingleItemHandler,editDataId}) => {
    const {done,name,type,count} = shopItem;
    const setEditDataHandler =()=>{  setEditData(shopItem); }

    return (
        <TouchableOpacity disabled={editMode} onLongPress={()=>onComplateSingleItemHandler(shopItem.id)}>
            <View style={[styles.container,{opacity: (done && !editMode) ? 0.5 : 1 }]} >
            {editMode &&
                <TouchableOpacity disabled={editDataId === shopItem.id} onPress={setEditDataHandler}>
                    <View style={[styles.editIcon,{opacity: editDataId === shopItem.id ? 0.5 : 1}]}>
                        <MaterialIcons  name="edit" size={24} color="white" />
                    </View>
                </TouchableOpacity>
            }
                <View style={[styles.innerContainer,{width:editMode ? '81%' : "100%"} ]}>
                    <Text style={styles.singleItemText}>{name}</Text>
                    <Text style={styles.singleItemText}>{`x${count} ${type}`}</Text>
                </View>
            {editMode &&
                <TouchableOpacity>
                    <View style={styles.deleteIcon}>
                        <AntDesign onPress={()=>onDeleteSingleItemHandler(shopItem.id,shopItem.name)} name="close" size={24} color="white" />
                    </View>
                </TouchableOpacity>
            }
            </View>
        </TouchableOpacity>
    )
}
const styles= StyleSheet.create({
    container:{flexDirection:'row',alignItems:'center',borderWidth:2,borderColor:COLORS.primary,borderRadius:27,
                marginBottom:14,maxHeight:40},
    innerContainer:{height:40,alignItems:'center',width:'81%',flexDirection:'row',justifyContent:'space-between',paddingHorizontal:19},
    singleItemText:{fontSize:14,fontFamily:'MontserratMedium',color:COLORS.black},
    deleteIcon:{backgroundColor:COLORS.main,borderRadius:19,height:38,width:36,justifyContent:'center',alignItems:'center'},
    editIcon:{backgroundColor:COLORS.secondary,borderRadius:19,height:38,width:36,justifyContent:'center',alignItems:'center'}
    
})
