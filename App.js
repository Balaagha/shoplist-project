import React,{Component} from "react";
import {StyleSheet,StatusBar } from "react-native";
import { Provider } from "react-redux";

import store from "./store";
import { RootNav } from "./navigation";

import { AppLoading } from "expo";
import { loadFonts } from "./styles/fonts";

export default class App extends Component {
  state ={loaded:false}
  render() {
    if (!this.state.loaded) {
      return ( <AppLoading  startAsync={loadFonts}  onFinish={() => this.setState({loaded:true})} 
                onError={() => console.log("Loading Rejected")}  /> );
    }
    return (
      <Provider store={store}>
        <StatusBar hidden={true}/>
        <RootNav/>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({ container: { flex: 1},});
