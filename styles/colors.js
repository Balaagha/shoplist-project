const COLORS = {
    main: "#FF7676",
    primary: "#FFE194",
    secondary: "#FFD976",
    black: "#303234",
    gray: "#EEEEEE",
    textColor:'#cccccc'
  };
  
  export default COLORS;
  