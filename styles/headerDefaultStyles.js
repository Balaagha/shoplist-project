import COLORS from "./colors";


export const headerDefaultStyles = Object.freeze({
    headerTitleAlign:'center',headerStyle:{backgroundColor:COLORS.main,height:73, elevation:0,shadowOpacity:0},
    headerTintColor:'#fff',headerTitleStyle: {fontFamily:'MontserratMedium',fontWeight: '600',fontSize:18},
    headerTitleAlign:'center',
});